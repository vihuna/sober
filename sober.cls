%% Sober LaTeX Class
%% v0.2, 2019/02/13
%% Víctor Huertas, http://reduccionalabsurdo.es
%%
%% Required packages: etoolbox, geometry, graphics, fancyhdr,
%%                    titlesec, ams*, hyperref.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{sober}[2019/02/13 v0.2 Sober]
\typeout{Sober Document Class}
\typeout{v0.2, 2019/02/13}
\typeout{Víctor Huertas}


% Load required packages to process class options.

% \RequirePackage{kvoptions} % not used yet
% \RequirePackage{ifthen} % obsolete, use etoolbox instead
\RequirePackage{etoolbox}


% TODO: Handle "titlepage option"

\DeclareOption*{%
  \PassOptionsToClass{\CurrentOption}{article}
}

\ProcessOptions\relax
\LoadClass[a4paper]{article}


\RequirePackage[T1]{fontenc}
\RequirePackage[utf8]{inputenc}

\RequirePackage[a4paper]{geometry}
\geometry{tmargin=2.5cm,bmargin=2.5cm,lmargin=2.5cm,rmargin=2.5cm,headsep=0.5cm,
headheight=15pt,footskip=1.5cm}


\RequirePackage{graphicx}
\RequirePackage[dvipsnames]{xcolor}


% Customization of page headers with 'fancyhdr'

\RequirePackage{fancyhdr}
\pagestyle{fancy}
\fancyhf{}
\fancyhead[L]{\leftmark}       
\fancyhead[R]{\thepage}



\RequirePackage{amsmath,amsthm,amssymb,amsfonts}


% TODO: modify title for "titlepage" option
% A more refined way to print the Document info (unfinished)

\newbool{AuthorFilled}
\global\boolfalse{AuthorFilled}
\let\tmpauthor\author
\renewcommand{\author}[1]{%
  \tmpauthor{#1}
  \ifblank{#1}{}{\global\booltrue{AuthorFilled}}
}

%\newbool{DateFilled} % Not used at this time
\newbool{DateUsed}
%\global\boolfalse{DateFilled}
\global\booltrue{DateUsed}

\let\tmpdate\date
\renewcommand{\date}[1]{%
  \tmpdate{#1}
  \ifblank{#1}{\global\boolfalse{DateUsed}}{}
}

\newbool{ContactUsed}
\global\boolfalse{ContactUsed}

\newcommand{\contact}[1]{%
  \newcommand{\@context}{#1}%
  \global\booltrue{ContactUsed}%
}


\newbool{LicUsed}
\global\boolfalse{LicUsed}

\newcommand{\license}[1]{%
  \newcommand{\@lictext}{#1}%
  \global\booltrue{LicUsed}%
}

\newlength{\DocInfoWidth}
\AtBeginDocument{%
  \settowidth{\DocInfoWidth}{\@author, \@date}
  %\settowidth{\TitleWidth}{\huge\sffamily\bfseries\@title}
}


% Customization of document title

\renewcommand{\maketitle}{%
    \par\noindent{\rule{\textwidth}{0.2ex}}%
    \par\vspace{4ex}
    \par\vspace{5ex}%
    \par\noindent\parbox[c]{\textwidth}{\huge\sffamily\bfseries\@title}%
    \vspace{1ex}\par\noindent{\rule{\textwidth}{0.6ex}}%
    \begin{flushright}%
      \ifboolexpr{bool {DateUsed} or bool {AuthorFilled} or bool {LicUsed} %
        or bool{ContactUsed} }{%
        \vspace{3ex}\par\noindent\rule{0.45\textwidth}{0.05ex}%
      }{}%
      \ifboolexpr{bool {DateUsed} or bool {AuthorFilled}}{%
        \vspace{3ex}%
      }{}%
      \ifbool{AuthorFilled}{%
        \par\noindent\@author%
      }{}%
      \ifbool{ContactUsed}{\par\noindent\@context}{}
      \ifbool{LicUsed}{\par\noindent\@lictext}{}
      \ifbool{DateUsed}{%
        \par\noindent\@date%
      }{}%
      %\par\noindent\rule{\dimexpr\DocInfoWidth+4ex\relax}{0.05ex}\par%
      \ifboolexpr{bool {DateUsed} or bool {AuthorFilled} or bool {LicUsed}
        or bool{ContactUsed} }{%
        \vspace{-1.5ex}\par\noindent\rule{0.45\textwidth}{0.05ex}%
      }{}
    \end{flushright}%
    \par\vspace{10ex}\par%
    \thispagestyle{plain}%
}


% Customization of section titles with 'titlesec' package

\RequirePackage{titlesec}

\titleformat{\section}{\Large\sffamily\bfseries}{\thesection}{6pt}{}
\titleformat{\subsection}{\large\sffamily\bfseries}{\thesubsection}{6pt}{}                             

% TODO: Configure theorem environments 
% TODO: Add frames for theorems
% TODO: Add environment for code listing


% Use custom configuration for hyperref package if loaded

\AtBeginDocument{
  \@ifpackageloaded{hyperref}{%
    \hypersetup{ pdfborder={0 0 0},colorlinks=true,allcolors=Red  }%
  }%
  {}
}


\endinput
